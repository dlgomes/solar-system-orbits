# Solar Symfony *v1*
>###### *orbit generator on symfony 5.3*
---

### Installation
>###### follow this steps to install it
---

1. clone | fork
2. run `composer install`
3. run `yarn | npm install`
4. run `php bin/console solar:install` to start the required data.
5. run `php bin/console solar:demo` to populate demo data *(optional)* 
7. play the *galileo*
---
---
---