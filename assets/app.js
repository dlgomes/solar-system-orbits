/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import './styles/app.scss';
import CustomRangeFieldManipulator from './modules/customRangeField'
import ModalHandler from "./modules/modalDisclaimer";

window.Modal = ModalHandler;
// jQuery & plugins
window.$ = window.jQuery;
$ = jQuery;
require("jquery.cookie");
// end of jQuery & plugins injection

$.cookie.defaults = { expires: 1 };

$(document).ready(function (){
    CustomRangeFieldManipulator.updateRangeValue();
    window.Modal.openModal();
});