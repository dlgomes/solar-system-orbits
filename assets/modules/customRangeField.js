export default class RangeFieldLabelManipulator {
    static updateRangeValue() {
        const $rangeFieldContainer = $('.field-angle');

        for (let x = 0; x < $rangeFieldContainer.length; x++) {
            const $innerRangeContainer = $($rangeFieldContainer[x]);
            const $innerRange = $innerRangeContainer.find('input');
            const $span = $innerRangeContainer.find('span:last-of-type');
            $span.html($innerRange.val());
        }

        $rangeFieldContainer.on('change', function() {
            const $range = $(this).find('input');
            const $span = $(this).find('span:last-of-type');
            $span.html($range.val());
        });
    }
}