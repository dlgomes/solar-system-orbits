<?php

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallDemoData extends Command
{
    protected static $defaultName = 'solar:demo';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $dropCommand = $this->getApplication()->find('doctrine:fixtures:load');
        $dropCommand->run($input, $output);
        return Command::SUCCESS;
    }
}
