<?php

namespace App\Command;

use App\Entity\Star;
use App\Entity\User;
use App\Repository\StarRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SolarInstall extends Command
{
    protected static $defaultName = 'solar:setup';

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserPasswordHasherInterface
     */
    private $hasher;
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var StarRepository
     */
    private $starRepository;

    public function __construct(UserRepository $userRepository, UserPasswordHasherInterface $hasher, EntityManagerInterface $objectManager, StarRepository $starRepository)
    {
        $this->userRepository = $userRepository;
        $this->starRepository = $starRepository;
        $this->hasher = $hasher;
        $this->objectManager = $objectManager;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->makeAdmin();

        $output->writeln(
            [
                "Admin created.",
                "-----",
                "username: demo",
                "password: demo",
                "-----",
                "generating demo Sun"
            ]
        );

        $this->makeSun();
        $this->objectManager->flush();

        $output->writeln([
            "all data created",
            "-----"
        ]);

        return Command::SUCCESS;

    }


    private function makeAdmin() {
        if (count($this->userRepository->findAll()) > 0) { return; }

        $demoAdmin = new User();
        $demoAdmin->setUsername("demo");
        $demoAdmin->setRoles(["ROLE_ADMIN"]);
        $demoAdmin->setPassword($this->hashPassword($demoAdmin));

        $this->objectManager->persist($demoAdmin);
    }

    private function makeSun() {
        if (count($this->starRepository->findAll()) > 0) { return; }

        $demoStar = new Star();
        $demoStar->setName("Sun");
        $this->objectManager->persist($demoStar);
    }

    private function hashPassword(User $user): string
    {
        $defaultPassword = "demo";
        return $this->hasher->hashPassword($user, $defaultPassword);
    }
}