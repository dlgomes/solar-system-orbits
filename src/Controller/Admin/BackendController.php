<?php

namespace App\Controller\Admin;

use App\Entity\Moon;
use App\Entity\Planet;
use App\Entity\Star;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackendController extends AbstractDashboardController
{
    /**
     * @Route("/backend", name="backend")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Symfony 5 Orbits | Backend');
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
                    ->addCssFile('build/app.css')
                    ->addWebpackEncoreEntry('app');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Users', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Stars', 'fas fa-star', Star::class);
        yield MenuItem::linkToCrud('Planets', 'fas fa-globe', Planet::class);
        yield MenuItem::linkToCrud('Moons', 'fas fa-moon', Moon::class);
    }
}