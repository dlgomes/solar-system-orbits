<?php

namespace App\Controller\Admin;

use App\Admin\Field\RangeField;
use App\Entity\Planet;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PlanetCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Planet::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            BooleanField::new('active'),

            TextField::new('name')->setColumns(6),

            AssociationField::new('star')
                ->setColumns(6)
                ->setRequired(true),

            RangeField::new(
                'angle',
                "Angle <span class='margin__left--sm'></span>(<span>0</span>°)",
                [
                    "label_html" => true,
                    "attr" => [
                        "max" => 360,
                        "min" => 0
                    ]
                ]
            )->setColumns(6),

            RangeField::new(
                'velocity',
                "Velocity <span class='margin__left--sm'></span>(<span>0</span>km/s)",
                [
                    "label_html" => true,
                    "attr" => [
                        "max" => "75",
                        "min" => "1"
                    ]
                ])->setColumns(6),

            RangeField::new(
                'orbital_radius',
                "Orbital Radius <span class='margin__left--sm'></span>(<span></span>) Millions of Km",
                [
                    "label_html" => true,
                    "attr" => [
                        "max" => 15000,
                        "min" => 1000
                    ]
                ]
            )->setColumns(6),

            RangeField::new(
                'diameter',
                "Diameter <span class='margin__left--sm'></span>(<span></span>) Kms",
                [
                    "label_html" => true,
                    "attr" => [
                        "max" => 750,
                        "min" => 200
                    ]
                ]
            )->setColumns(6),

            ChoiceField::new('rotation')
                ->setColumns(6)
                ->setChoices( ["left" => "left", "right" => "right"] ),

            ColorField::new('color')->setColumns(6),
        ];
    }
}
