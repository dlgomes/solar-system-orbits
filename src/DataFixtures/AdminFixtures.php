<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AdminFixtures extends Fixture implements OrderedFixtureInterface
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $hasher;
    /**
     * @var null
     */
    private $manager;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->manager = null;
        $this->hasher = $passwordHasher;
    }

    public function getOrder(): int
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->createDemoAdmin($manager);
        $manager->flush();
    }

    /**
     * PRIVATE METHODS
     */

    /**
     * NOTE: Generates Hashed Password based on given user
     * @param User $user
     * @return string
     */
    private function hashPassword(User $user): string
    {
        $defaultPassword = "demo";
        return $this->hasher->hashPassword($user, $defaultPassword);
    }

    /**
     * @param ObjectManager $manager
     */
    private function createDemoAdmin(ObjectManager $manager)
    {
        $demoAdmin = new User();
        $demoAdmin->setUsername("demo");
        $demoAdmin->setRoles(["ROLE_ADMIN"]);
        $demoAdmin->setPassword($this->hashPassword($demoAdmin));

        $this->manager->persist($demoAdmin);
    }
}