<?php

namespace App\DataFixtures;

use App\Entity\Moon;
use App\Entity\Planet;
use App\Repository\PlanetRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MoonFixtures extends Fixture implements OrderedFixtureInterface
{

    private $planetRepository;
    /**
     * @var null
     */
    private $manager;

    public function __construct(PlanetRepository $planetRepository)
    {
        $this->manager = null;
        $this->planetRepository = $planetRepository;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->populateMoons();
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 5;
    }

    private function populateMoons(): void
    {
        $moonData = $this->getMoonData();
        // loop planet association data for moons
        foreach ($moonData as $planetName => $moonDataArray) {
            // loop independent moon data
            foreach ($moonDataArray as $moonData) {
                $planet = $this->getPlanet($planetName);
                $moonData['planet'] = $planet;
                $moon = new Moon();
                $moon->populateFromArray($moonData);
                $this->manager->persist($moon);
            }
        }
    }

    private function getPlanet($planetName): Planet
    {
        return $this->planetRepository->findOneBy(["name" => $planetName]);
    }

    private function getMoonData(): array
    {
        return [
            "Earth" => [
                [
                    "planet" => null,
                    "name" => "Moon",
                    "angle" => rand(0, 359),
                    "velocity" => 25,
                    "rotation" => "right",
                    "color" => "#ede7d8",
                    "orbital_radius" => 5
                ],
            ],
            "Mars" => [
                [
                    "planet" => null,
                    "name" => "Phobos",
                    "angle" => rand(0, 359),
                    "velocity" => 75,
                    "rotation" => "right",
                    "color" => "#dbc697",
                    "orbital_radius" => 5
                ],
                [
                    "planet" => null,
                    "name" => "Deimos",
                    "angle" => rand(0, 359),
                    "velocity" => 40,
                    "rotation" => "right",
                    "color" => "#d6cebc",
                    "orbital_radius" => 7
                ],
            ],
            "Jupiter" => [
                [
                    "planet" => null,
                    "name" => "Europa",
                    "angle" => rand(0, 359),
                    "velocity" => 60,
                    "rotation" => "right",
                    "color" => "#d19292",
                    "orbital_radius" => 5
                ],
                [
                    "planet" => null,
                    "name" => "Ganymede",
                    "angle" => rand(0, 359),
                    "velocity" => 40,
                    "rotation" => "right",
                    "color" => "#e8e8e3",
                    "orbital_radius" => 7
                ],
                [
                    "planet" => null,
                    "name" => "Io",
                    "angle" => rand(0, 359),
                    "velocity" => 20,
                    "rotation" => "right",
                    "color" => "#f0ea9c",
                    "orbital_radius" => 9
                ],
            ]
        ];
    }
}