<?php

namespace App\DataFixtures;

use App\Entity\Star;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class StarFixtures extends Fixture implements OrderedFixtureInterface
{
    /**
     * @var null
     */
    private $manager;

    public function __construct()
    {
        $this->manager = null;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->populateStars($manager);
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 3;
    }

    private function populateStars(ObjectManager $manager)
    {
        $demoStar = new Star();
        $demoStar->setName("Sun");
        $this->manager->persist($demoStar);
    }
}