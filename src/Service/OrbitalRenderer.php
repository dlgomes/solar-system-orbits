<?php

namespace App\Service;

use App\Entity\Star;
use App\Repository\MoonRepository;
use App\Repository\PlanetRepository;
use App\Repository\StarRepository;
use Symfony\Component\HttpFoundation\Response;

class OrbitalRenderer
{
    /**
     * @var StarRepository
     */
    private $starRepository;
    /**
     * @var PlanetRepository
     */
    private $planetRepository;
    /**
     * @var MoonRepository
     */
    private $moonRepository;

    public function __construct(StarRepository $starRepository, PlanetRepository $planetRepository, MoonRepository $moonRepository)
    {
        $this->starRepository = $starRepository;
        $this->planetRepository = $planetRepository;
        $this->moonRepository = $moonRepository;
    }

    /**
     * @return array
     */
    public function getPlanets(): array
    {
        return $this->planetRepository->findAll();
    }

    /**
     * @return array
     */
    public function getMoons(): array
    {
        return $this->moonRepository->findAll();
    }

    /**
     * @return Star[]
     */
    public function getStar(): array
    {
        return $this->starRepository->findBy([]);
    }

    /**
     * @return false | array
     */
    public function getDumpForRootView()
    {
        $star = $this->getStar();
        if (!$star || !$star[0] || count($star) > 1) { return false; }


        return [
            "star" => $star[0],
            "planets" => $this->getPlanets(),
            "moons" => $this->getMoons()
        ];
    }
}