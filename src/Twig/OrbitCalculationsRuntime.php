<?php

namespace App\Twig;

use App\Entity\Planet;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class OrbitCalculationsRuntime extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('getVmin', [$this, 'calculateVmin']),
            new TwigFilter('getOrbitalTime', [$this, 'calculateOrbitalTime']),
            new TwigFilter('getCorpVmin', [$this, 'calculateCelestialCorpVmin']),
            new TwigFilter('getStartAngle', [$this, 'getStartAngleFromCelestialCorp']),
            new TwigFilter('getEndAngle', [$this, 'getEndAngleFromCelestialCorp']),
        ];
    }

    /**
     * @param $celestialCorp
     * @return float
     */
    public function calculateVmin($celestialCorp): float
    {
        $className = $this->em->getClassMetadata(get_class($celestialCorp))->getName();
        $maxVmin = $className === "App\Entity\Planet" ? 47 : 3.5;

        if (get_class($celestialCorp) == Planet::class) {
            $furtherDistance = 15000;
        } else {
            $furtherDistance = 10;
        }
        return round(($maxVmin * $celestialCorp->getOrbitalRadius()) / $furtherDistance, 2);
    }

    /**
     * @param $celestialCorp
     * @return float
     */
    public function calculateOrbitalTime($celestialCorp): float
    {
        $perimeter = $this->getOrbitalPerimeter($celestialCorp);
        $velocity = $celestialCorp->getVelocity();

        if (get_class($celestialCorp) == Planet::class) {
            $leverage = 0.055;
        } else {
            $leverage = 3.333;
        }


        return round(($perimeter / $velocity) * $leverage, 2);
    }

    /**
     * @param $celestialCorp
     * @return float
     */
    public function calculateCelestialCorpVmin($celestialCorp): float
    {
        $maxVmin = 10.420;
        $celestialCorpType = get_class($celestialCorp);
        $biggestCelestialCorp = $this->em->getRepository($celestialCorpType)->findBy([], ["diameter" => "DESC"])[0];
        $biggestDiameter = $biggestCelestialCorp->getDiameter();

        if (get_class($celestialCorp) == Planet::class) {
            $maxVmin = $maxVmin * 0.755;
        }

        return round(($maxVmin * $celestialCorp->getDiameter()) / $biggestDiameter, 2);
    }

    /**
     * @param $celestialCorp
     * @return float
     */
    public function getStartAngleFromCelestialCorp($celestialCorp): float
    {
        return $celestialCorp->getAngle();
    }

    /**
     * @param $celestialCorp
     * @return float
     */
    public function getEndAngleFromCelestialCorp($celestialCorp): float
    {
        if ($celestialCorp->getRotation() === "right") {
            $finalAngle = $celestialCorp->getAngle() + 360;
        } else {
            $finalAngle = $celestialCorp->getAngle() - 360;
        }

        return $finalAngle;
    }

    /**
     * NOTE: Retrieves Orbital perimeter of given celestial corp
     * @param $celestialCorp
     * @return float
     */
    private function getOrbitalPerimeter($celestialCorp): float
    {
        return 2 * (pi() * $celestialCorp->getOrbitalRadius());
    }
}